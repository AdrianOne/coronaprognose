#include <stdio.h>
#include <iostream>
#include <math.h>
#include <string>
#include <fstream>
#include <algorithm>
#include <vector>
#include <sstream> 
#include <iomanip>

struct DataSet {
    std::string day;
    std::string infected;
    std::string dead;
    std::string infected_factor;
    std::string dead_factor;
    // Function used in "calculate_factor" to return required member
    std::string getInfectedOrDead(int infected_or_dead) {
        if (infected_or_dead == 1) {
            return infected;
        } else {
            return dead;
        }
    }
};

// Declares Methods
// Defaultly "prognose_days" is "-1", which means it only displays current data and not prognose data
// Defaultly "start_prognose_day" is "-1", because it is optional and only used when prognose_days is not "-1"
void display_data(const std::string& file_name, int prognose_days = -1, int start_prognose_day = -1);

// Splits string at delimeter and returns a vector
std::vector<std::string> split_string(const std::string& s, char delim) {
    std::string string_token;
    std::vector<std::string> string_vector;
    for (int i = 0; i < s.length(); i++) {
        if (s[i] == delim) {
            string_vector.push_back(string_token);
            string_token = "";
        } else {
            string_token += s[i];
        }
    }
    string_vector.push_back(string_token);
    
    return string_vector;
}

// Calculates average Multiply-Factor (for infections and deads)
//TODO: CHANGE CALCULATION OF AVGFACTOR WITH SORTING OUT EXTREMES
double calculate_avg_factor(std::vector<DataSet>& data_set_vector, int infected_or_dead, int start_prognose_day) {
    double avg_factor = 0;
    double last_count;
    int ignored_number = 0;
    
    /* The start from which the calculations should begin is subtracted by "2" because:
     * 1. the list starts at index "0" and the variable counts normal
     * 2. the variable "last_count" needs one number before the current to calculate the factor
     * When the calculation start is "1" the user propably wants all numbers included in the calculation,
     * but because a factor can't be calculated for the first data set, the start of the calculation is changed to "2" 
    */
    
    if (start_prognose_day == 1) {
        start_prognose_day = 2;
    }
    
    start_prognose_day -= 2;
    
    // Sets one number before the wished start for "last_count"
    last_count = std::stoi(data_set_vector[start_prognose_day].getInfectedOrDead(infected_or_dead));
    // Sums averages of counts (starting at one because for the first data set (index zero) a factor can't be calculated)
    for (int i = 1 + start_prognose_day; i < data_set_vector.size(); i++) {  
        // Checks if last number is zero to prevent a division through zero
        if (last_count != 0) {
            avg_factor += std::stoi(data_set_vector[i].getInfectedOrDead(infected_or_dead)) / last_count;
            // Sets current count to "last_count" so in the next iteration the factor can be calculated and added to "avg_factor"
            last_count = std::stoi(data_set_vector[i].getInfectedOrDead(infected_or_dead));
        } else {
            // Increment counter for times zero people were dead or when a number has been ignored because it was too small
            ignored_number++;
        }
        last_count = stoi(data_set_vector[i].getInfectedOrDead(infected_or_dead));
    }
    // Calculates average factor ("-1" because for the first data set a factor can't be calculated)
    avg_factor = avg_factor / (data_set_vector.size() - ignored_number - start_prognose_day - 1);
    
    return avg_factor;
}

// Gets number of lines in the data file
int get_line_count(const std::string& file_name) {
    int line_count = 0;
    std::string line;
    std::ifstream file(file_name);
    
    // Sums count of lines
    while(std::getline(file, line)) {
        line_count++;
    }
    file.close();
    return line_count;
}

std::string convert_to_percent(const std::string& factor_string) {
    std::string result;
    std::stringstream factor_stream;
    double factor;
    // Only positive numbers are needed, because this program only shows infections and deaths which don't decline in numbers
    if (factor_string != "n/a") {
        // Convert "factor_string" to a double called "factor"
        factor = std::stod(factor_string);
        // When the factor is "1" the factor in percent is "+0.00%"
        if (factor == 1) {
            result = "+0.00%";
        } else if (factor < 2) {
            factor_stream << std::fixed << std::setprecision(2) << ((factor - 1) * 100);
            result = "+" + factor_stream.str() + "%";
        } else if (factor >= 2) {
            factor_stream << std::fixed << std::setprecision(2) << (factor * 100);
            result = "+" + factor_stream.str() + "%";
        }
    } else {
        result = factor_string;
    }
    
    return result;
}

// Calculates prognose
std::vector<std::string> calculate_prognose(const std::string& file_name, int prognose_days, int start_prognose_day) {
    double avg_infected_factor;
    double avg_dead_factor;
    double infected = 0;
    double dead = 0;
    std::vector<std::string> prognose;
    
    std::vector<DataSet> data_set_vector;
    DataSet data_set;
    
    std::ifstream file(file_name);
    std::string line;
    
    int line_count = get_line_count(file_name);
    
    // Checks if file exists/is empty
    if (file.peek() == std::ifstream::traits_type::eof()) {
        std::cout << "\033[1;31m[ERROR]\033[0m File is empty. Please input data!" << std::endl;
        return prognose;
    }
    // Checks how many datasets the file contains, and outputs an error message,
    // if only one exists (because it is impossible to calculate a prognose with only one dataset).
    if (line_count == 1) {
        std::cout << "\033[1;31m[ERROR]\033[0m File contains only one data set. Please input more data!" << std::endl;
        return prognose;
    }
    
    if (line_count < start_prognose_day) {
        std::cout << "\033[1;31m[ERROR]\033[0m Too big number for the start day has been inputted. Please choose a start day below " 
        << "\033[33m" << line_count << "\033[0m" 
        << " or exactly " 
        << "\033[33m" << line_count << "\033[0m" 
        << "." << std::endl;
        
        return prognose;
    }
    
    // Gets lines of file, splits them and puts them in a dataset-vector
    while(std::getline(file, line)) {
        std::vector<std::string> splitted_string = split_string(line, ':');
        
        data_set.day = splitted_string[0];
        data_set.infected = splitted_string[1];
        data_set.dead = splitted_string[2];
        
        data_set_vector.push_back(data_set);
    }
    
    // Calculates average factors
    avg_infected_factor = calculate_avg_factor(data_set_vector, 1, start_prognose_day);
    avg_dead_factor = calculate_avg_factor(data_set_vector, 2, start_prognose_day);
    
    // Calculates prognoses (exponentially)
    infected = stoi(data_set_vector[data_set_vector.size() - 1].infected) * pow(avg_infected_factor, prognose_days);
    dead = stoi(data_set_vector[data_set_vector.size() - 1].dead) * pow(avg_dead_factor, prognose_days);
    
    // Casts calculated doubles in a int
    prognose = { 
        std::to_string((int) infected), 
        std::to_string((int) dead), 
        convert_to_percent(std::to_string(avg_infected_factor)), 
        convert_to_percent(std::to_string(avg_dead_factor)) 
    };
    
    return prognose;
}

// Writes to file
void write_file(const std::string& file_name, const std::string& content) {
    std::ofstream output_file;
    
    output_file.open(file_name, std::ios_base::app);
    output_file << content << std::endl;
    output_file.close();
}

// Gets longest number in data to display table correctly
int get_longest_length(std::vector<DataSet>& data_set_vector, int index) {
    int result;
    std::vector<std::string> splitted_data_set = { 
        data_set_vector[0].day, 
        data_set_vector[0].infected, 
        data_set_vector[0].dead,
        data_set_vector[0].infected_factor, 
        data_set_vector[0].dead_factor };
    // Sets first value of data as result
    result = splitted_data_set[index].length();
    for(DataSet data_set: data_set_vector) {
        splitted_data_set[0] = data_set.day;
        splitted_data_set[1] = data_set.infected;
        splitted_data_set[2] = data_set.dead;
        splitted_data_set[3] = data_set.infected_factor;
        splitted_data_set[4] = data_set.dead_factor;
        // Checks if the current value is higher than the current highest value
        if (splitted_data_set[index].length() > result) {
            result = splitted_data_set[index].length();
        }
    }
    return result;
}

std::string calculate_factor(const std::string& file_name, int todays_day, int index) {
    std::string result;
    std::string line;
    std::vector<std::string> splitted_string;
    
    int yesterdays_day;
    
    DataSet yesterdays_data_set;
    DataSet todays_data_set;
    
    std::ifstream file(file_name);
    
    while (std::getline(file, line)) {
        splitted_string = split_string(line, ':');
        
        todays_data_set.day = splitted_string[0];
        todays_data_set.infected = splitted_string[1];
        todays_data_set.dead = splitted_string[2];
        
        if (std::stoi(todays_data_set.day) == todays_day) {
            break;
        }

    }
    file.close();
    
    yesterdays_day = todays_day - 1;
    // Proceeds when the day of the needed factor isn't the first
    if (yesterdays_day != 0) {
        file.open(file_name);
        
        while (std::getline(file, line)) {
            splitted_string = split_string(line, ':');
            
            yesterdays_data_set.day = splitted_string[0];
            yesterdays_data_set.infected = splitted_string[1];
            yesterdays_data_set.dead = splitted_string[2];
            
            if (std::stoi(yesterdays_data_set.day) == yesterdays_day) {
                break;
            }
        }
        
        file.close();
        
        // Proceeds when the day of the needed factor isn't the first which is not zero
        if (std::stoi(yesterdays_data_set.getInfectedOrDead(index)) != 0) {
            // Calculates factor
            result = std::to_string(std::stod(todays_data_set.getInfectedOrDead(index)) / std::stod(yesterdays_data_set.getInfectedOrDead(index)));
        } else {
            result = "n/a";
        }
    } else {
        result = "n/a";
    }
    return result;
}

// Copys data from file into "DataSet-Vector" 
std::vector<DataSet> get_data(const std::string& file_name, int prognose_days, int start_prognose_day) {
    std::string line;
    std::ifstream file;
    std::vector<DataSet> data_set_vector;
    std::vector<std::string> prognose_vector; 
    // Checks if file exists/is empty and outputs error message
    file.open(file_name);
    
    if (file.peek() == std::ifstream::traits_type::eof()) {
        std::cout << "\033[1;31m[ERROR]\033[0m File is empty. Please input data!" << std::endl;
        return data_set_vector;
    }
    
    // Gets lines of file, splits them and puts them in a vector
    while(getline(file, line)) {
        std::vector<std::string> splitted_string = split_string(line, ':');
        // Sets members of struct 
        DataSet data_set;
        data_set.day = splitted_string[0];
        data_set.infected = splitted_string[1];
        data_set.dead = splitted_string[2]; 
        data_set.infected_factor = convert_to_percent(calculate_factor(file_name, std::stoi(splitted_string[0]), 1));
        data_set.dead_factor = convert_to_percent(calculate_factor(file_name, std::stoi(splitted_string[0]), 2));
        
        // Add struct to vector
        data_set_vector.push_back(data_set);
    } 
    file.close();
    
    // Appends prognosed numbers to data_set_vector when needed
    if (prognose_days != -1) {
        DataSet prognose_data_set;
        std::string line;
        int current_day;
        
        file.open(file_name);
        // Gets latest day and continues with the next
        while(getline(file, line)) {
            std::vector<std::string> splitted_string = split_string(line, ':');
            current_day = stoi(splitted_string[0]) + 1;
        }
        file.close();
        for(int i = 1; i <= prognose_days; i++) {
            // FIXME: CREATE EXTRA STRING TO ADD COLOR TO VALUES
            prognose_vector = calculate_prognose(file_name, i, start_prognose_day);
            
            prognose_data_set.day = std::to_string(current_day) + " (P)";
            prognose_data_set.infected = prognose_vector[0];
            prognose_data_set.dead = prognose_vector[1];
            prognose_data_set.infected_factor = prognose_vector[2];
            prognose_data_set.dead_factor = prognose_vector[3];
            
            data_set_vector.push_back(prognose_data_set);
            current_day++;
        }
    }
    
    return data_set_vector;
}

// FIXME: Maybe add those display functions to a own class

std::string display_data_value(const std::string& data_set_value, const std::string& description, int width) {
    std::string result;
    
    result = "|" + data_set_value + 
    std::string((data_set_value.length() < description.length())
    ? ((width < description.length()) ? description.length() - data_set_value.length() : width - data_set_value.length()) 
    : width - data_set_value.length(), ' ');
    
    return result;
}

std::string display_description(const std::string& color_string, const std::string& description, int width) {
    std::string result;
    
    result = "|" + color_string + description + "\033[0m" + 
    std::string((width < description.length()) ? 0 : width - description.length(), ' ');
    
    return result;
}

void display_data(const std::string& file_name, int prognose_days, int start_prognose_day) {
    int width_day;
    int width_infected; 
    int width_dead;
    int width_infected_factor;
    int width_dead_factor;
    int current_day;
    
    std::string day = "Day";
    std::string infected = "Infected";
    std::string dead = "Dead";
    std::string infected_factor = "InfectedFactor";
    std::string dead_factor = "DeadFactor";
    
    std::vector<DataSet> data_set_vector = get_data(file_name, prognose_days, start_prognose_day);
    
    // The size of "data_set_vector" is zero when the file is empty. The error message for this is outputted in the "get_data()" method. 
    if (data_set_vector.size() == 0) {
        return;
    }

    // Get longest length of each column
    width_day = get_longest_length(data_set_vector, 0);
    width_infected = get_longest_length(data_set_vector, 1);
    width_dead = get_longest_length(data_set_vector, 2);
    width_infected_factor = get_longest_length(data_set_vector, 3);
    width_dead_factor = get_longest_length(data_set_vector, 4);
    
    // Displays upper border in correct count with ternary operators
    std::cout << std::string(
      ((width_day < day.length()) ? 0 : width_day - day.length()) 
    + ((width_infected < infected.length()) ? 0 : width_infected - infected.length())
    + ((width_infected_factor < infected_factor.length()) ? 0 : width_infected_factor - infected_factor.length())
    + ((width_dead < dead.length()) ? 0 : width_dead - dead.length())
    + ((width_dead_factor < dead_factor.length()) ? 0 : width_dead_factor - dead_factor.length()) 
    + day.length() + infected.length() + dead.length() + infected_factor.length() + dead_factor.length() + 6, '_') << std::endl;
    
    // Displays description and seperator in correct count with ternary operators
    std::cout <<
    display_description("\033[1;32m", day, width_day) <<
    display_description("\033[1;33m", infected, width_infected) <<
    display_description("\033[1;33m", infected_factor, width_infected_factor) <<
    display_description("\033[1;31m", dead, width_dead) <<
    display_description("\033[1;31m", dead_factor, width_dead_factor) <<
    "|" << std::endl;
    
    // Displays data with correct count of spaces with ternary operators
    for(DataSet data_set: data_set_vector) {
        
        std::cout << 
        display_data_value(data_set.day, day, width_day) <<
        display_data_value(data_set.infected, infected, width_infected) <<
        display_data_value(data_set.infected_factor, infected_factor, width_infected_factor) <<
        display_data_value(data_set.dead, dead, width_dead) <<
        display_data_value(data_set.dead_factor, dead_factor, width_dead_factor) << 
        "|" << std::endl;
        
    }
}

void display_prognose(const std::string& file_name) {
    int prognose_days;
    int start_prognose_day;
    // FIXME: ADD EXCEPTION WHEN STRING IS INPUTTED
    std::cout << "How many days to you want to prognose?: ";
    std::cin >> prognose_days;
    std::cout << "At which day do you want to start the prognose?: ";
    std::cin >> start_prognose_day;
    // Calculates prognose out of submitted days
    std::vector<std::string> prognose = calculate_prognose(file_name, prognose_days, start_prognose_day);
    // The size of "vector_list" is zero when the file is empty or only one data sets exists. The error message for this is outputted in the "get_data()" method. 
    if (prognose.size() == 0) {
        return;
    }
    // Displays current data + prognose
    display_data(file_name, prognose_days, start_prognose_day);
    
    std::cout << "\033[1;33mInfections: \033[0m" << prognose[0] << std::endl;
    std::cout << "\033[1;31mDeaths: \033[0m" << prognose[1] << std::endl;
}

void input_data(const std::string& file_name) {
    std::string line;
    std::string infected_and_dead;
    std::string data_line;
    int current_day;
    std::vector<std::string> splitted_string;
    DataSet data_set;
    std::ifstream file(file_name);
    
    // Checks if file exists/is empty and start with day 1, if so
    if (file.peek() == std::ifstream::traits_type::eof()) {
        current_day = 1;
    } else {
        // Gets last line of file
        while(getline(file, line)) {
            // Gets last day and continues with the next
            current_day = stoi(split_string(line, ':')[0]) + 1;
        }
    }
    file.close();
    while (true) {
        std::cout << "Day " << current_day << ": Enter new data or quit (Q): ";
        std::cin >> infected_and_dead;
        if (infected_and_dead == "q") {
            break;
        }
        // Splits string
        splitted_string = split_string(infected_and_dead, ':');
        
        // Checks if data is correctly inputted
        int user_input_size = splitted_string.size();
        if (user_input_size < 2 || user_input_size > 2 ) {
            std::cout << "\033[1;31m[ERROR]\033[0m Invalid Input! Please enter data in this form: 'infected:dead' " << std::endl;
            continue;
        }
        
        data_set.day = std::to_string(current_day);
        data_set.infected = splitted_string[0];
        data_set.dead = splitted_string[1];
        
        data_line = data_set.day + ":" + data_set.infected + ":" + data_set.dead;
        
        // Writes data to file
        write_file(file_name, data_line);
        current_day++;
    }
}

void edit_data(const std::string& file_name) {
    std::cout << "NOT FINISHED" << std::endl;
}

void main_loop(const std::string& file_name) {
    std::string use_choice;
    // Loop to get to main menu when leaving a sub menu
    while (true) {
        // Gets choice
        std::cout << "Do you want to make a prognose (P), input new data (I), show current data (D), edit data (E) or quit (Q)?: ";
        std::cin >> use_choice;
        // Transforms choice to lowercase to ignore case
        std::transform(use_choice.begin(), use_choice.end(), use_choice.begin(), ::tolower);
        // Prognose
        if (use_choice == "p") {
            display_prognose(file_name);
        // Input new data
        } else if (use_choice == "i") {
            input_data(file_name);
        // Display data
        } else if (use_choice == "d") {
            display_data(file_name); 
        // Edit data
        } else if (use_choice == "e") {
            edit_data(file_name);
        // Quit
        } else if (use_choice == "q") {
            break;
        } else {
            // Displays error message when invalid choice is submitted
            std::cout << "\033[1;31m[ERROR]\033[0m Invalid Input!" << std::endl;
        }
    }
}

int main(int argc, char **argv) {
    // Variable for filename
    std::string file_name = "corona_numbers.dat";
    main_loop(file_name);
    return 0;
}
